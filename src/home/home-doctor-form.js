import React from 'react';
import Button from "react-bootstrap/Button";
import APIResponseErrorMessage from "../commons/errorhandling/api-response-error-message";
import {Col, FormGroup, Input, Label, Row} from "reactstrap";
import * as API_USERS from "../doctor/api/doctor-api";


class HomeDoctorForm extends React.Component {

    constructor(props) {
        super(props);
        this.toggleForm = this.toggleForm.bind(this);

        this.state = {

            errorStatus: 0,
            error: null,

            formIsValid: false,

            formControls: {
                email: {
                    value: '',
                    placeholder: 'email',
                    touched: false,
                },
                parola: {
                    value: '',
                    placeholder: 'parola',
                    touched: false,
                },
            }
        };

        this.handleChange = this.handleChange.bind(this);
        this.handleSubmitDoctor = this.handleSubmitDoctor.bind(this);
    }

    toggleForm() {
        this.setState({collapseForm: !this.state.collapseForm});
    }


    handleChange = event => {

        const name = event.target.name;
        const value = event.target.value;

        const updatedControls = this.state.formControls;

        const updatedFormElement = updatedControls[name];

        updatedFormElement.value = value;
        updatedFormElement.touched = true;
        updatedControls[name] = updatedFormElement;

        let formIsValid = true;
        for (let updatedFormElementName in updatedControls) {
            formIsValid = updatedControls[updatedFormElementName].valid && formIsValid;
        }

        this.setState({
            formControls: updatedControls,
            formIsValid: formIsValid
        });

    };

    searchDoctorAccount(home) {
        return API_USERS.getDoctors((result, status, err) => {

            if (result !== null && status === 200) {
                let ok=0;
                for (const res in result){
                    console.log(result[res].email+home.email+result[res].password+home.parola);
                    if(result[res].email===home.email&&result[res].password===home.parola){
                        window.close();
                        window.open('http://localhost:3000/doctor-page');
                        localStorage["doctor"]=result[res].name;
                        localStorage["doctorAuth"]="1";
                        ok=1;
                    }
                }
                if(ok===0)alert("Email sau parola gresite.");
            } else {
                this.setState(({
                    errorStatus: status,
                    error: err
                }));
            }
        });
    }

    handleSubmitDoctor() {
        let cont = {
            email: this.state.formControls.email.value,
            parola: this.state.formControls.parola.value,
        };

        console.log(cont);
        this.searchDoctorAccount(cont);
    }

    render() {
        return (
            <div>

                <FormGroup id='email'>
                    <Label for='emailField'> Email: </Label>
                    <Input name='email' id='emailField' placeholder={this.state.formControls.email.placeholder}
                           onChange={this.handleChange}
                           defaultValue={this.state.formControls.email.value}
                           touched={this.state.formControls.email.touched? 1 : 0}
                           required
                    />
                </FormGroup>

                <FormGroup id='parola'>
                    <Label for='parolaField'> Parola: </Label>
                    <Input name='parola' id='parolaField' placeholder={this.state.formControls.parola.placeholder}
                           onChange={this.handleChange}
                           defaultValue={this.state.formControls.parola.value}
                           touched={this.state.formControls.parola.touched? 1 : 0}
                           required
                    />
                </FormGroup>

                    <Row>
                        <Col sm={{size: '4', offset: 8}}>
                            <Button type={"submit"} onClick={this.handleSubmitDoctor}>  Conecteaza-te </Button>
                        </Col>
                    </Row>

                {
                    this.state.errorStatus > 0 &&
                    <APIResponseErrorMessage errorStatus={this.state.errorStatus} error={this.state.error}/>
                }
            </div>
        ) ;
    }
}

export default HomeDoctorForm;
