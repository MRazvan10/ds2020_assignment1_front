import React from 'react';

import BackgroundImg from '../commons/images/future-medicine.jpg';

import {Button, Container, Jumbotron, Modal, ModalBody, ModalHeader} from 'reactstrap';
import HomePacientForm from "./home-pacient-form";
import HomeDoctorForm from "./home-doctor-form";
import HomeIngrijitorForm from "./home-ingrijitor-form";

const backgroundStyle = {
    backgroundPosition: 'center',
    backgroundSize: 'cover',
    backgroundRepeat: 'no-repeat',
    width: "100%",
    height: "1920px",
    backgroundImage: `url(${BackgroundImg})`
};
const textStyle = {color: 'black', };

class Home extends React.Component {
    constructor(props) {
        super(props);
        this.toggleFormPacient = this.toggleFormPacient.bind(this);
        this.toggleFormDoctor = this.toggleFormDoctor.bind(this);
        this.toggleFormIngrijitor = this.toggleFormIngrijitor.bind(this);
        this.state = {
            selectedPacient: false,
            selectedDoctor: false,
            selectedIngrijitor: false,
            collapseForm: false,
            tableData: [],
            isLoaded: false,
            errorStatus: 0,
            error: null
        };
    }
    toggleFormDoctor() {
        this.setState({selectedDoctor: !this.state.selectedDoctor});
    }

    toggleFormIngrijitor() {
        this.setState({selectedIngrijitor: !this.state.selectedIngrijitor});
    }

    toggleFormPacient() {
        this.setState({selectedPacient: !this.state.selectedPacient});
    }

    render() {

        return (

            <div>
                <Jumbotron fluid style={backgroundStyle}>
                    <Container fluid>
                        <h1 className="display-3" style={textStyle}>Platforma spital</h1>

                        <p  style={textStyle}> <b>Pentru a va conecta apasati pe unul din urmatoarele butoane.</b> </p>
                        <p className="lead">
                            <Button color="primary" onClick={this.toggleFormDoctor}>Doctor</Button>
                        </p>

                        <p className="lead">
                            <Button color="primary" onClick={this.toggleFormIngrijitor}>Ingrijitor</Button>
                        </p>

                        <p className="lead">
                            <Button color="primary" onClick={this.toggleFormPacient}>Pacient</Button>
                        </p>
                    </Container>
                </Jumbotron>

                <Modal isOpen={this.state.selectedPacient} toggle={this.toggleFormPacient}
                       className={this.props.className} size="lg">
                    <ModalHeader toggle={this.toggleFormPacient}> Intra ca si pacient: </ModalHeader>
                    <ModalBody>
                        <HomePacientForm/>
                    </ModalBody>
                </Modal>

                <Modal isOpen={this.state.selectedDoctor} toggle={this.toggleFormDoctor}
                       className={this.props.className} size="lg">
                    <ModalHeader toggle={this.toggleFormDoctor}> Intra ca si doctor: </ModalHeader>
                    <ModalBody>
                        <HomeDoctorForm/>
                    </ModalBody>
                </Modal>

                <Modal isOpen={this.state.selectedIngrijitor} toggle={this.toggleFormIngrijitor}
                       className={this.props.className} size="lg">
                    <ModalHeader toggle={this.toggleFormIngrijitor}> Intra ca si ingrijitor: </ModalHeader>
                    <ModalBody>
                        <HomeIngrijitorForm/>
                    </ModalBody>
                </Modal>
            </div>
        )
    };
}

export default Home
