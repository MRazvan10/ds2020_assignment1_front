import React from 'react';
import Button from "react-bootstrap/Button";
import APIResponseErrorMessage from "../commons/errorhandling/api-response-error-message";
import {Col, Row} from "reactstrap";
import { FormGroup, Input, Label} from 'reactstrap';
import * as API_USERS from "../pacient/api/pacient-api";

class HomePacientForm extends React.Component {

    constructor(props) {
        super(props);
        this.toggleForm = this.toggleForm.bind(this);

        this.state = {

            errorStatus: 0,
            error: null,

            formIsValid: false,

            formControls: {
                email: {
                    value: '',
                    placeholder: 'email',
                    touched: false,
                },
                parola: {
                    value: '',
                    placeholder: 'parola',
                    touched: false,
                },
            }
        };

        this.handleChange = this.handleChange.bind(this);
        this.handleSubmitPacient = this.handleSubmitPacient.bind(this);
    }

    toggleForm() {
        this.setState({collapseForm: !this.state.collapseForm});
    }


    handleChange = event => {

        const name = event.target.name;
        const value = event.target.value;

        const updatedControls = this.state.formControls;

        const updatedFormElement = updatedControls[name];

        updatedFormElement.value = value;
        updatedFormElement.touched = true;
        updatedControls[name] = updatedFormElement;

        let formIsValid = true;
        for (let updatedFormElementName in updatedControls) {
            formIsValid = updatedControls[updatedFormElementName].valid && formIsValid;
        }

        this.setState({
            formControls: updatedControls,
            formIsValid: formIsValid
        });

    };

    searchPacientAccount(home) {
        return API_USERS.getPacients((result, status, err) => {

            if (result !== null && status === 200) {
                let ok=0;
                for (const res in result){
                    console.log(result[res].email+home.email+result[res].password+home.parola);
                    if(result[res].email===home.email&&result[res].password===home.parola){
                        window.close();
                        window.open('http://localhost:3000/pacient-page');
                        localStorage["pacientId"]=result[res].id;
                        localStorage["pacientAuth"]="1";
                        localStorage["pacientName"]=result[res].name;
                        localStorage["pacientEmail"]=result[res].email;
                        localStorage["pacientPassword"]=result[res].password;
                        localStorage["pacientAddress"]=result[res].address;
                        localStorage["pacientAge"]=result[res].age;
                        localStorage["pacientIdIngrijitor"]=result[res].idIngrijitor;
                        localStorage["pacientDiagnostic"]=result[res].diagnostic;
                        console.log(result[res].tratament)
                        localStorage["pacientTratament"]=result[res].tratament;
                        ok=1;
                    }
                }
                if(ok===0)alert("Email sau parola gresite.");
            } else {
                this.setState(({
                    errorStatus: status,
                    error: err
                }));
            }
        });
    }
    handleSubmitPacient() {
        let cont = {
            email: this.state.formControls.email.value,
            parola: this.state.formControls.parola.value,
        };

        console.log(cont);
        this.searchPacientAccount(cont);
    }

    render() {
        return (
            <div>

                <FormGroup id='email'>
                    <Label for='emailField'> Email: </Label>
                    <Input name='email' id='emailField' placeholder={this.state.formControls.email.placeholder}
                           onChange={this.handleChange}
                           defaultValue={this.state.formControls.email.value}
                           touched={this.state.formControls.email.touched? 1 : 0}
                           required
                    />
                </FormGroup>

                <FormGroup id='parola'>
                    <Label for='parolaField'> Parola: </Label>
                    <Input name='parola' id='parolaField' placeholder={this.state.formControls.parola.placeholder}
                           onChange={this.handleChange}
                           defaultValue={this.state.formControls.parola.value}
                           touched={this.state.formControls.parola.touched? 1 : 0}
                           required
                    />
                </FormGroup>

                    <Row>
                        <Col sm={{size: '4', offset: 8}}>
                            <Button type={"submit"} onClick={this.handleSubmitPacient}>  Conecteaza-te </Button>
                        </Col>
                    </Row>

                {
                    this.state.errorStatus > 0 &&
                    <APIResponseErrorMessage errorStatus={this.state.errorStatus} error={this.state.error}/>
                }
            </div>
        ) ;
    }
}

export default HomePacientForm;
