import React from 'react'
import {BrowserRouter as Router, Route, Switch} from 'react-router-dom'
import NavigationBar from './navigation-bar'
import Home from './home/home';
import PacientContainer from './pacient/pacient-container'

import ErrorPage from './commons/errorhandling/error-page';
import styles from './commons/styles/project-style.css';
import DoctorContainer from "./doctor/doctor-container";
import ListDoctorContainer from "./doctor/list-doctor-container";
import IngrijitorContainer from "./ingrijitor/ingrijitor-container";
import ListIngrijitorContainer from "./ingrijitor/list-ingrijitor-container";
import DoctorPage from "./doctor-page/doctor-page";
import PacientPage from "./pacient-page/pacient-page";
import MedicamentContainer from "./medicament/medicament-container";
import ListMedicamentContainer from "./medicament/list-medicament-container";
import IngrijitorPageContainer from "./ingrijitor-page/ingrijitorPage-container";

class App extends React.Component {


    render() {

        return (
            <div className={styles.back}>
            <Router>
                <div>
                    <NavigationBar />
                    <Switch>

                        <Route
                            exact
                            path='/'
                            render={() => <Home/>}
                        />

                        <Route
                            exact
                            path='/pacient'
                            render={() => <PacientContainer/>}
                        />
                        <Route
                            exact
                            path='/medicament'
                            render={() => <MedicamentContainer/>}
                        />

                        <Route
                            exact
                            path='/medicamentlist'
                            render={() => <ListMedicamentContainer/>}
                        />

                        <Route
                            exact
                            path='/doctor-page'
                            render={() => <DoctorPage/>}
                        />

                        <Route
                            exact
                            path='/pacient-page'
                            render={() => <PacientPage/>}
                        />

                        <Route
                            exact
                            path='/ingrijitor-page'
                            render={() => <IngrijitorPageContainer/>}
                        />

                        <Route
                            exact
                            path='/doctor'
                            render={() => <DoctorContainer/>}
                        />

                        <Route
                            exact
                            path='/doctorlist'
                            render={() => <ListDoctorContainer/>}
                        />

                        <Route
                            exact
                            path='/ingrijitor'
                            render={() => <IngrijitorContainer/>}
                        />

                        <Route
                            exact
                            path='/ingrijitorlist'
                            render={() => <ListIngrijitorContainer/>}
                        />

                        {/*Error*/}
                        <Route
                            exact
                            path='/error'
                            render={() => <ErrorPage/>}
                        />

                        <Route render={() =><ErrorPage/>} />
                    </Switch>
                </div>
            </Router>
            </div>
        )
    };
}

export default App
