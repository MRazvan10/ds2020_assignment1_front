import {HOST} from '../../commons/hosts';
import RestApiClient from "../../commons/api/rest-client";


const endpoint = {
    medicament: '/medicament'
};

function getMedicaments(callback) {
    let request = new Request(HOST.backend_api + endpoint.medicament, {
        method: 'GET',
    });
    console.log(request.url);
    RestApiClient.performRequest(request, callback);
}

function getMedicamentById(params, callback){
    let request = new Request(HOST.backend_api + endpoint.medicament + params.id, {
       method: 'GET'
    });

    console.log(request.url);
    RestApiClient.performRequest(request, callback);
}

function postMedicament(user, callback){
    let request = new Request(HOST.backend_api + endpoint.medicament , {
        method: 'POST',
        headers : {
            'Accept': 'application/json',
            'Content-Type': 'application/json',
        },
        body: JSON.stringify(user)
    });

    console.log("URL: " + request.url);

    RestApiClient.performRequest(request, callback);
}

function deleteMedicament(params, callback){
    let request = new Request(HOST.backend_api + endpoint.medicament +'/' +params.id, {
        method: 'DELETE'
    });

    console.log(request.url);
    RestApiClient.performRequest(request, callback);
}
function updateMedicament(user, callback){
    let request = new Request(HOST.backend_api + endpoint.medicament+'/'+user.id , {
        method: 'PUT',
        headers : {
            'Accept': 'application/json',
            'Content-Type': 'application/json',
        },
        body: JSON.stringify(user)
    });

    console.log("URL: " + request.url);

    RestApiClient.performRequest(request, callback);
}
export {
    getMedicaments,
    getMedicamentById,
    postMedicament,
    deleteMedicament,
    updateMedicament
};
