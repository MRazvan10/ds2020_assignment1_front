import React from "react";
import Table from "../../commons/tables/table";
import ReactTable from "react-table";


const columns = [
    {
        Header: 'Name',
        accessor: 'name',
    },
    {
        Header: 'Descriere',
        accessor: 'descriere',
        width: 400
    },
    {
        Header: 'Id',
        accessor: 'id',
        width: 400
    }
];

const filters = [
    {
        accessor: 'name',
    }
];

const onRowClick = (state, rowInfo) => {
    return {
        onClick: e => {
            console.log(rowInfo.original.id);
            localStorage['updateMedicamentId']=rowInfo.original.id;
            localStorage['updateMedicamentName']=rowInfo.original.name;
            localStorage['updateMedicamentDescriere']=rowInfo.original.descriere;
        }
    }
}

class MedicamentTable extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            tableData: this.props.tableData
        };
    }

    render() {
        return (
            <ReactTable
                data={this.state.tableData}
                columns={columns}
                search={filters}
                getTrProps={onRowClick}
                pageSize={5}
            />
        )
    }
}

export default MedicamentTable;