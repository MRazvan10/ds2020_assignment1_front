import React from 'react';
import APIResponseErrorMessage from "../commons/errorhandling/api-response-error-message";
import {
    Button,
    Card,
    CardHeader,
    Col,
    Modal,
    ModalBody,
    ModalHeader,
    Row
} from 'reactstrap';
import MedicamentForm from "./components/medicament-form";
import MedicamentForm2 from "./components/medicament-form2";
import MedicamentForm3 from "./components/medicament-form3";

import * as API_USERS from "./api/medicament-api"
import MedicamentTable from "./components/medicament-table";



class MedicamentContainer extends React.Component {

    constructor(props) {
        super(props);
        this.toggleForm = this.toggleForm.bind(this);
        this.toggleForm2 = this.toggleForm2.bind(this);
        this.toggleForm3 = this.toggleForm3.bind(this);
        this.reload = this.reload.bind(this);
        this.state = {
            selected: false,
            selected2: false,
            selected3: false,
            collapseForm: false,
            tableData: [],
            isLoaded: false,
            errorStatus: 0,
            error: null
        };
    }

    componentDidMount() {
        this.fetchMedicaments();
    }

    fetchMedicaments() {
        return API_USERS.getMedicaments((result, status, err) => {

            if (result !== null && status === 200) {
                this.setState({
                    tableData: result,
                    isLoaded: true
                });
            } else {
                this.setState(({
                    errorStatus: status,
                    error: err
                }));
            }
        });
    }

    toggleForm() {
        this.setState({selected: !this.state.selected});
    }
    toggleForm2() {
        this.setState({selected2: !this.state.selected2});
    }

    toggleForm3() {
        this.setState({selected3: !this.state.selected3});
    }



    reload() {
        this.setState({
            isLoaded: false
        });
        if(this.state.selected) this.toggleForm();
        if(this.state.selected2) this.toggleForm2();
        if(this.state.selected3) this.toggleForm3();
        this.fetchMedicaments();
    }

    render() {
        if(localStorage["doctorAuth"]==="1")
        return (
            <div>
                <CardHeader>
                    <strong> Medicament Management </strong>
                </CardHeader>
                <Card>
                    <br/>
                    <Row>
                        <Col sm={{size: '8', offset: 1}}>
                            <Button color="primary" onClick={this.toggleForm}>Add Medicament </Button>
                            <strong>  </strong>
                            <Button color="primary" onClick={this.toggleForm2}>Delete Medicament </Button>
                            <strong>  </strong>
                            <Button color="primary" onClick={this.toggleForm3}>Update Medicament </Button>
                        </Col>
                    </Row>
                    <br/>
                    <Row>
                        <Col sm={{size: '8', offset: 1}}>
                            {this.state.isLoaded && <MedicamentTable tableData = {this.state.tableData}/>}
                            {this.state.errorStatus > 0 && <APIResponseErrorMessage
                                                            errorStatus={this.state.errorStatus}
                                                            error={this.state.error}
                                                        />   }
                        </Col>
                    </Row>
                </Card>

                <Modal isOpen={this.state.selected} toggle={this.toggleForm}
                       className={this.props.className} size="lg">
                    <ModalHeader toggle={this.toggleForm}> Add Medicament: </ModalHeader>
                    <ModalBody>
                        <MedicamentForm reloadHandler={this.reload}/>
                    </ModalBody>
                </Modal>

                <Modal isOpen={this.state.selected2} toggle={this.toggleForm2}
                       className={this.props.className} size="lg">
                    <ModalHeader toggle={this.toggleForm2}> Delete Medicament: </ModalHeader>
                    <ModalBody>
                        <MedicamentForm2 reloadHandler={this.reload}/>
                    </ModalBody>
                </Modal>

                <Modal isOpen={this.state.selected3} toggle={this.toggleForm3}
                       className={this.props.className} size="lg">
                    <ModalHeader toggle={this.toggleForm3}> Update Medicament: </ModalHeader>
                    <ModalBody>
                        <MedicamentForm3 reloadHandler={this.reload}/>
                    </ModalBody>
                </Modal>
            </div>
        )
        else{return null;}

    }
}


export default MedicamentContainer;
