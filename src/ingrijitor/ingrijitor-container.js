import React from 'react';
import APIResponseErrorMessage from "../commons/errorhandling/api-response-error-message";
import {
    Button,
    Card,
    CardHeader,
    Col,
    Modal,
    ModalBody,
    ModalHeader,
    Row
} from 'reactstrap';
import IngrijitorForm from "./components/ingrijitor-form";
import IngrijitorForm2 from "./components/ingrijitor-form2";
import IngrijitorForm3 from "./components/ingrijitor-form3";

import * as API_USERS from "./api/ingrijitor-api"
import IngrijitorTable from "./components/ingrijitor-table";



class IngrijitorContainer extends React.Component {

    constructor(props) {
        super(props);
        this.toggleForm = this.toggleForm.bind(this);
        this.toggleForm2 = this.toggleForm2.bind(this);
        this.toggleForm3 = this.toggleForm3.bind(this);
        this.reload = this.reload.bind(this);
        this.state = {
            selected: false,
            selected2: false,
            selected3: false,
            collapseForm: false,
            tableData: [],
            isLoaded: false,
            errorStatus: 0,
            error: null
        };
    }

    componentDidMount() {
        this.fetchIngrijitors();
    }

    fetchIngrijitors() {
        return API_USERS.getIngrijitors((result, status, err) => {

            if (result !== null && status === 200) {
                this.setState({
                    tableData: result,
                    isLoaded: true
                });
            } else {
                this.setState(({
                    errorStatus: status,
                    error: err
                }));
            }
        });
    }

    toggleForm() {
        this.setState({selected: !this.state.selected});
    }

    toggleForm2() {
        this.setState({selected2: !this.state.selected2});
    }

    toggleForm3() {
        this.setState({selected3: !this.state.selected3});
    }


    reload() {
        this.setState({
            isLoaded: false
        });
        if(this.state.selected) this.toggleForm();
        if(this.state.selected2) this.toggleForm2();
        if(this.state.selected3) this.toggleForm3();
        this.fetchIngrijitors();
    }

    render() {
        if(localStorage["doctorAuth"]==="1")
        return (
            <div>
                <CardHeader>
                    <strong> Ingrijitor Management </strong>
                </CardHeader>
                <Card>
                    <br/>
                    <Row>
                        <Col sm={{size: '8', offset: 1}}>
                            <Button color="primary" onClick={this.toggleForm}>Add Ingrijitor </Button>
                            <strong>  </strong>
                            <Button color="primary" onClick={this.toggleForm2}>Delete Ingrijitor </Button>
                            <strong>  </strong>
                            <Button color="primary" onClick={this.toggleForm3}>Update Ingrijitor </Button>
                        </Col>
                    </Row>
                    <br/>
                    <Row>
                        <Col sm={{size: '8', offset: 1}}>
                            {this.state.isLoaded && <IngrijitorTable tableData = {this.state.tableData}/>}
                            {this.state.errorStatus > 0 && <APIResponseErrorMessage
                                                            errorStatus={this.state.errorStatus}
                                                            error={this.state.error}
                                                        />   }
                        </Col>
                    </Row>
                </Card>

                <Modal isOpen={this.state.selected} toggle={this.toggleForm}
                       className={this.props.className} size="lg">
                    <ModalHeader toggle={this.toggleForm}> Add Ingrijitor: </ModalHeader>
                    <ModalBody>
                        <IngrijitorForm reloadHandler={this.reload}/>
                    </ModalBody>
                </Modal>

                <Modal isOpen={this.state.selected2} toggle={this.toggleForm2}
                       className={this.props.className} size="lg">
                    <ModalHeader toggle={this.toggleForm2}> Delete Ingrijitor: </ModalHeader>
                    <ModalBody>
                        <IngrijitorForm2 reloadHandler={this.reload}/>
                    </ModalBody>
                </Modal>

                <Modal isOpen={this.state.selected3} toggle={this.toggleForm3}
                       className={this.props.className} size="lg">
                    <ModalHeader toggle={this.toggleForm3}> Update Ingrijitor: </ModalHeader>
                    <ModalBody>
                        <IngrijitorForm3 reloadHandler={this.reload}/>
                    </ModalBody>
                </Modal>

            </div>
        )
        else{return null;}

    }
}


export default IngrijitorContainer;
