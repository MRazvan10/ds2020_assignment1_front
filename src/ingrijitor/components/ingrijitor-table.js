import React from "react";
import Table from "../../commons/tables/table";
import ReactTable from "react-table";


const columns = [
    {
        Header: 'Name',
        accessor: 'name',
    },
    {
        Header: 'Email',
        accessor: 'email',
    },
    {
        Header: 'Id',
        accessor: 'id',
        width: 400
    }
];

const filters = [
    {
        accessor: 'name',
    }
];

const onRowClick = (state, rowInfo) => {
    return {
        onClick: e => {
            console.log(rowInfo.original.id);
            localStorage['updateIngrijitorId']=rowInfo.original.id;
            localStorage['updateIngrijitorName']=rowInfo.original.name;
            localStorage['updateIngrijitorEmail']=rowInfo.original.email;;
        }
    }
}

class IngrijitorTable extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            tableData: this.props.tableData
        };
    }

    render() {
        return (
            <ReactTable
                data={this.state.tableData}
                columns={columns}
                search={filters}
                getTrProps={onRowClick}
                pageSize={5}
            />
        )
    }
}

export default IngrijitorTable;