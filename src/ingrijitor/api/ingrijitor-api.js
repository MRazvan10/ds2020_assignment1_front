import {HOST} from '../../commons/hosts';
import RestApiClient from "../../commons/api/rest-client";


const endpoint = {
    ingrijitor: '/ingrijitor'
};

function getIngrijitors(callback) {
    let request = new Request(HOST.backend_api + endpoint.ingrijitor, {
        method: 'GET',
    });
    console.log(request.url);
    RestApiClient.performRequest(request, callback);
}

function getIngrijitorById(params, callback){
    let request = new Request(HOST.backend_api + endpoint.ingrijitor +'/' +params.id, {
       method: 'GET'
    });

    console.log(request.url);
    RestApiClient.performRequest(request, callback);
}

function postIngrijitor(user, callback){
    let request = new Request(HOST.backend_api + endpoint.ingrijitor , {
        method: 'POST',
        headers : {
            'Accept': 'application/json',
            'Content-Type': 'application/json',
        },
        body: JSON.stringify(user)
    });

    console.log("URL: " + request.url);

    RestApiClient.performRequest(request, callback);
}

function deleteIngrijitor(params, callback){
    let request = new Request(HOST.backend_api + endpoint.ingrijitor +'/' +params.id, {
        method: 'DELETE'
    });

    console.log(request.url);
    RestApiClient.performRequest(request, callback);
}

function updateIngrijitor(user, callback){
    let request = new Request(HOST.backend_api + endpoint.ingrijitor+'/'+user.id , {
        method: 'PUT',
        headers : {
            'Accept': 'application/json',
            'Content-Type': 'application/json',
        },
        body: JSON.stringify(user)
    });

    console.log("URL: " + request.url);

    RestApiClient.performRequest(request, callback);
}
export {
    getIngrijitors,
    getIngrijitorById,
    postIngrijitor,
    deleteIngrijitor,
    updateIngrijitor
};
