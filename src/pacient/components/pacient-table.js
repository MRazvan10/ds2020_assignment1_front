import React from "react";
import Table from "../../commons/tables/table";
import ReactTable from "react-table";
import PacientForm from "./pacient-form";
import {ModalBody} from "reactstrap";


const columns = [
    {
        Header: 'Id',
        accessor: 'id',
        width: 400
    },
    {
        Header: 'Name',
        accessor: 'name',
        width: 200
    },
    {
        Header: 'Age',
        accessor: 'age',
    },
    {
        Header: 'Diagnostic',
        accessor: 'diagnostic',
        width: 400
    },
    {
        Header: 'Address',
        accessor: 'address',
        width: 400
    },
    {
        Header: 'Email',
        accessor: 'email',
        width: 400
    },
    {
        Header: 'idIngrijitor',
        accessor: 'idIngrijitor',
        width: 400
    },
    {
        Header: 'Tratament',
        accessor: 'tratament',
        width: 400
    }
];

const filters = [
    {
        accessor: 'name',
    }
];

const onRowClick = (state, rowInfo) => {
    return {
        onClick: e => {
            console.log(rowInfo.original.id);
            localStorage['updatePacientId']=rowInfo.original.id;
            localStorage['updatePacientName']=rowInfo.original.name;
            localStorage['updatePacientEmail']=rowInfo.original.email;
            localStorage['updatePacientDiagnostic']=rowInfo.original.diagnostic;
            localStorage['updatePacientAddress']=rowInfo.original.address;
            localStorage['updatePacientIdIngrijitor']=rowInfo.original.idIngrijitor;
            localStorage['updatePacientAge']=rowInfo.original.age;
            localStorage['updatePacientTratament']=rowInfo.original.tratament;
        }
    }
}

class PacientTable extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            tableData: this.props.tableData
        };
    }

    render() {
        return (
            <ReactTable
                data={this.state.tableData}
                columns={columns}
                search={filters}
                getTrProps={onRowClick}
                pageSize={5}
            />
        )
    }
}

export default PacientTable;