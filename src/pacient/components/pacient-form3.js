import React from 'react';
import validate from "./validators/pacient-validators";
import Button from "react-bootstrap/Button";
import * as API_USERS from "../api/pacient-api";
import APIResponseErrorMessage from "../../commons/errorhandling/api-response-error-message";
import {Col, Row} from "reactstrap";
import { FormGroup, Input, Label} from 'reactstrap';



class PacientForm3 extends React.Component {

    constructor(props) {
        super(props);
        this.toggleForm = this.toggleForm.bind(this);
        this.reloadHandler = this.props.reloadHandler;

        this.state = {

            errorStatus: 0,
            error: null,

            formIsValid: false,

            formControls: {
                name: {
                    value: localStorage['updatePacientName'],
                    placeholder: 'What is your name?...',
                    valid: false,
                    touched: false,
                    validationRules: {
                        minLength: 3,
                        isRequired: true
                    }
                },
                email: {
                    value: localStorage['updatePacientEmail'],
                    placeholder: 'Email...',
                    valid: false,
                    touched: false,
                    validationRules: {
                        emailValidator: true
                    }
                },
                age: {
                    value: localStorage['updatePacientAge'],
                    placeholder: 'Age...',
                    valid: false,
                    touched: false,
                },
                address: {
                    value: localStorage['updatePacientAddress'],
                    placeholder: 'Cluj, Zorilor, Str. Lalelelor 21...',
                    valid: false,
                    touched: false,
                },
                diagnostic: {
                    value: localStorage['updatePacientDiagnostic'],
                    placeholder: 'Diagnostic...',
                    valid: false,
                    touched: false,
                },
                idIngrijitor: {
                    value: localStorage['updatePacientIdIngrijitor'],
                    placeholder: 'ID-ul ingrijitorului...',
                    valid: false,
                    touched: false,
                },
                tratament: {
                    value: localStorage['updatePacientTratament'],
                    placeholder: '',
                    valid: false,
                    touched: false,
                },
            }
        };

        this.handleChange = this.handleChange.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
    }

    toggleForm() {
        this.setState({collapseForm: !this.state.collapseForm});
    }


    handleChange = event => {

        const name = event.target.name;
        const value = event.target.value;

        const updatedControls = this.state.formControls;

        const updatedFormElement = updatedControls[name];

        updatedFormElement.value = value;
        updatedFormElement.touched = true;
        updatedFormElement.valid = validate(value, updatedFormElement.validationRules);
        updatedControls[name] = updatedFormElement;

        let formIsValid = true;
        for (let updatedFormElementName in updatedControls) {
            formIsValid = updatedControls[updatedFormElementName].valid && formIsValid;
        }

        this.setState({
            formControls: updatedControls,
            formIsValid: formIsValid
        });

    };

    registerPacient(pacient) {
        return API_USERS.updatePacient(pacient, (result, status, error) => {
            if (result !== null && (status === 200 || status === 201)) {
                console.log("Successfully inserted pacient with id: " + result);
                this.reloadHandler();
            } else {
                this.setState(({
                    errorStatus: status,
                    error: error
                }));
            }
        });
    }

    handleSubmit() {
        let pacient = {
            id: localStorage['updatePacientId'],
            name: this.state.formControls.name.value,
            email: this.state.formControls.email.value,
            age: this.state.formControls.age.value,
            address: this.state.formControls.address.value,
            tratament: this.state.formControls.tratament.value,
            diagnostic: this.state.formControls.diagnostic.value,
            idIngrijitor: this.state.formControls.idIngrijitor.value,
        };

        API_USERS.getPacientById(pacient,(result, status, error) => {
            if (result !== null) {
                console.log(result.password);
                localStorage['updatePacientPassword']=result.password;
            }
        });

        let pacient2 = {
            id: localStorage['updatePacientId'],
            name: this.state.formControls.name.value,
            email: this.state.formControls.email.value,
            age: this.state.formControls.age.value,
            address: this.state.formControls.address.value,
            tratament: this.state.formControls.tratament.value,
            diagnostic: this.state.formControls.diagnostic.value,
            idIngrijitor: this.state.formControls.idIngrijitor.value,
            password: localStorage['updatePacientPassword']
        };

        console.log(pacient2);
        this.registerPacient(pacient2);
    }

    render() {
        return (
            <div>

                <FormGroup id='name'>
                    <Label for='nameField'> Name: </Label>
                    <Input name='name' id='nameField' placeholder={this.state.formControls.name.placeholder}
                           onChange={this.handleChange}
                           defaultValue={this.state.formControls.name.value}
                           touched={this.state.formControls.name.touched? 1 : 0}
                           valid={this.state.formControls.name.valid}
                           required
                    />
                    {this.state.formControls.name.touched && !this.state.formControls.name.valid &&
                    <div className={"error-message row"}> * Name must have at least 3 characters </div>}
                </FormGroup>

                <FormGroup id='email'>
                    <Label for='emailField'> Email: </Label>
                    <Input name='email' id='emailField' placeholder={this.state.formControls.email.placeholder}
                           onChange={this.handleChange}
                           defaultValue={this.state.formControls.email.value}
                           touched={this.state.formControls.email.touched? 1 : 0}
                           valid={this.state.formControls.email.valid}
                           required
                    />
                    {this.state.formControls.email.touched && !this.state.formControls.email.valid &&
                    <div className={"error-message"}> * Email must have a valid format</div>}
                </FormGroup>

                <FormGroup id='address'>
                    <Label for='addressField'> Address: </Label>
                    <Input name='address' id='addressField' placeholder={this.state.formControls.address.placeholder}
                           onChange={this.handleChange}
                           defaultValue={this.state.formControls.address.value}
                           touched={this.state.formControls.address.touched? 1 : 0}
                           valid={this.state.formControls.address.valid}
                           required
                    />
                </FormGroup>

                <FormGroup id='age'>
                    <Label for='ageField'> Age: </Label>
                    <Input name='age' id='ageField' placeholder={this.state.formControls.age.placeholder}
                           min={0} max={100} type="number"
                           onChange={this.handleChange}
                           defaultValue={this.state.formControls.age.value}
                           touched={this.state.formControls.age.touched? 1 : 0}
                           valid={this.state.formControls.age.valid}
                           required
                    />
                </FormGroup>
                <FormGroup id='diagnostic'>
                    <Label for='diagnosticField'> Diagnostic: </Label>
                    <Input name='diagnostic' id='diagnosticField' placeholder={this.state.formControls.diagnostic.placeholder}
                           onChange={this.handleChange}
                           defaultValue={this.state.formControls.diagnostic.value}
                           touched={this.state.formControls.diagnostic.touched? 1 : 0}
                           valid={this.state.formControls.diagnostic.valid}
                           required
                    />
                </FormGroup>
                    <FormGroup id='idIngrijitor'>
                    <Label for='idIngrijitorField'> ID Ingrijitor: </Label>
                    <Input name='idIngrijitor' id='idIngrijitorField' placeholder={this.state.formControls.idIngrijitor.placeholder}
                           onChange={this.handleChange}
                           defaultValue={this.state.formControls.idIngrijitor.value}
                           touched={this.state.formControls.idIngrijitor.touched? 1 : 0}
                           valid={this.state.formControls.idIngrijitor.valid}
                           required
                    />
                </FormGroup>

                <FormGroup id='tratament'>
                    <Label for='tratamentField'>Tratament: </Label>
                    <Input name='tratament' id='tratamentField' placeholder={this.state.formControls.tratament.placeholder}
                           onChange={this.handleChange}
                           defaultValue={this.state.formControls.tratament.value}
                           touched={this.state.formControls.tratament.touched? 1 : 0}
                           valid={this.state.formControls.tratament.valid}
                           required
                    />
                </FormGroup>

                    <Row>
                        <Col sm={{size: '4', offset: 8}}>
                            <Button type={"submit"} onClick={this.handleSubmit}>  Submit </Button>
                        </Col>
                    </Row>

                {
                    this.state.errorStatus > 0 &&
                    <APIResponseErrorMessage errorStatus={this.state.errorStatus} error={this.state.error}/>
                }
            </div>
        ) ;
    }
}

export default PacientForm3;
