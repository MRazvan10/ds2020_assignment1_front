import React from "react";
import Table from "../../commons/tables/table";


const columns = [
    {
        Header: 'Name',
        accessor: 'name',
        width: 200
    },
    {
        Header: 'Age',
        accessor: 'age',
    },
    {
        Header: 'Diagnostic',
        accessor: 'diagnostic',
        width: 400
    },
    {
        Header: 'Address',
        accessor: 'address',
        width: 400
    },
    {
        Header: 'Email',
        accessor: 'email',
        width: 400
    },
    {
        Header: 'idIngrijitor',
        accessor: 'idIngrijitor',
        width: 400
    },
    {
        Header: 'Tratament',
        accessor: 'tratament',
        width: 400
    }
];

const filters = [
    {
        accessor: 'name',
    }
];

class IngrijitorPageTable extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            tableData: this.props.tableData
        };
    }

    render() {
        return (
            <Table
                data={this.state.tableData}
                columns={columns}
                search={filters}
                pageSize={5}
            />
        )
    }
}

export default IngrijitorPageTable;