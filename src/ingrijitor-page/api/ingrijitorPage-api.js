import {HOST} from '../../commons/hosts';
import RestApiClient from "../../commons/api/rest-client";


const endpoint = {
    pacient: '/pacient'
};

function getPacients(callback) {
    let request = new Request(HOST.backend_api + endpoint.pacient, {
        method: 'GET',
    });
    console.log(request.url);
    RestApiClient.performRequest(request, callback);
}

function getPacientById(params, callback){
    let request = new Request(HOST.backend_api + endpoint.pacient + params.id, {
       method: 'GET'
    });

    console.log(request.url);
    RestApiClient.performRequest(request, callback);
}

function postPacient(user, callback){
    let request = new Request(HOST.backend_api + endpoint.pacient , {
        method: 'POST',
        headers : {
            'Accept': 'application/json',
            'Content-Type': 'application/json',
        },
        body: JSON.stringify(user)
    });

    console.log("URL: " + request.url);

    RestApiClient.performRequest(request, callback);
}

export {
    getPacients,
    getPacientById,
    postPacient
};
