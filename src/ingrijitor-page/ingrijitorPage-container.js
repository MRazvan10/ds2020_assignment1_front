import React from 'react';
import APIResponseErrorMessage from "../commons/errorhandling/api-response-error-message";
import {
    Button,
    Card,
    CardHeader,
    Col,
    Modal,
    ModalBody,
    ModalHeader,
    Row
} from 'reactstrap';
import IngrijitorPageForm from "./components/ingrijitorPage-form";

import * as API_USERS from "./api/ingrijitorPage-api"
import IngrijitorPageTable from "./components/ingrijitorPage-table";


class IngrijitorPageContainer extends React.Component {

    constructor(props) {
        super(props);
        this.toggleForm = this.toggleForm.bind(this);
        this.reload = this.reload.bind(this);
        this.state = {
            selected: false,
            collapseForm: false,
            tableData: [],
            isLoaded: false,
            errorStatus: 0,
            error: null
        };
    }

    componentDidMount() {
        this.fetchIngrijitorPages();
    }

    fetchIngrijitorPages() {
        return API_USERS.getPacients((result, status, err) => {

            let result2=result;
            if (result !== null && status === 200) {
                for (const res in result){
                    if(result[res].idIngrijitor!==localStorage["ingrijitor"]){
                        delete result2[res];
                    }
                }
                this.setState({
                    tableData: result2,
                    isLoaded: true
                });
            } else {
                this.setState(({
                    errorStatus: status,
                    error: err
                }));
            }
        });
    }

    toggleForm() {
        this.setState({selected: !this.state.selected});
    }


    reload() {
        this.setState({
            isLoaded: false
        });
        this.toggleForm();
        this.fetchIngrijitorPages();
    }

    render() {
        if(localStorage["ingrijitorAuth"]==="1")
        return (
            <div>
                <CardHeader>
                    <strong> IngrijitorPage Management </strong>
                </CardHeader>
                <Card>
                    <br/>
                    <Row>
                        <Col sm={{size: '8', offset: 1}}>
                            <Button color="primary" onClick={this.toggleForm}>Add Pacient</Button>
                        </Col>
                    </Row>
                    <br/>
                    <Row>
                        <Col sm={{size: '8', offset: 1}}>
                            {this.state.isLoaded && <IngrijitorPageTable tableData = {this.state.tableData}/>}
                            {this.state.errorStatus > 0 && <APIResponseErrorMessage
                                                            errorStatus={this.state.errorStatus}
                                                            error={this.state.error}
                                                        />   }
                        </Col>
                    </Row>
                </Card>

                <Modal isOpen={this.state.selected} toggle={this.toggleForm}
                       className={this.props.className} size="lg">
                    <ModalHeader toggle={this.toggleForm}> Add Pacient: </ModalHeader>
                    <ModalBody>
                        <IngrijitorPageForm reloadHandler={this.reload}/>
                    </ModalBody>
                </Modal>

            </div>
        )
    else{return null;}

    }
}


export default IngrijitorPageContainer;
