import React from 'react';

import BackgroundImg from '../commons/images/future-medicine.jpg';

import {Button, Container, Jumbotron, Modal, ModalBody, ModalHeader} from 'reactstrap';

const backgroundStyle = {
    backgroundPosition: 'center',
    backgroundSize: 'cover',
    backgroundRepeat: 'no-repeat',
    width: "100%",
    height: "1920px",
    backgroundImage: `url(${BackgroundImg})`
};
const textStyle = {color: 'black', };

class PacientPage extends React.Component {

    render() {
        if(localStorage["pacientAuth"]==="1")
        return (
            <div>
                <Jumbotron fluid style={backgroundStyle}>
                    <Container fluid>
                        <h1 className="display-3" style={textStyle}>Bine ati venit, {localStorage["pacientName"]}</h1>

                        <p  style={textStyle}> <b>ID: {localStorage["pacientId"]}</b> </p>
                        <p  style={textStyle}> <b>Email: {localStorage["pacientEmail"]}</b> </p>
                        <p  style={textStyle}> <b>Password: {localStorage["pacientPassword"]}</b> </p>
                        <p  style={textStyle}> <b>Address: {localStorage["pacientAddress"]}</b> </p>
                        <p  style={textStyle}> <b>Age: {localStorage["pacientAge"]}</b> </p>
                        <p  style={textStyle}> <b>Id ingrijitor: {localStorage["pacientIdIngrijitor"]}</b> </p>
                        <p  style={textStyle}> <b>Diagnostic: {localStorage["pacientDiagnostic"]}</b> </p>
                        <p  style={textStyle}> <b>Tratament: {localStorage["pacientTratament"]}</b> </p>
                    </Container>
                </Jumbotron>
            </div>
        )
        else{return null;}
    };
}

export default PacientPage
