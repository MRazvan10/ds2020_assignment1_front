import React from 'react';

import BackgroundImg from '../commons/images/future-medicine.jpg';

import {Button, Container, Jumbotron, Modal, ModalBody, ModalHeader} from 'reactstrap';

const backgroundStyle = {
    backgroundPosition: 'center',
    backgroundSize: 'cover',
    backgroundRepeat: 'no-repeat',
    width: "100%",
    height: "1920px",
    backgroundImage: `url(${BackgroundImg})`
};
const textStyle = {color: 'black', };

class DoctorPage extends React.Component {

    render() {
        if(localStorage["doctorAuth"]==="1")
        return (
            <div>
                <Jumbotron fluid style={backgroundStyle}>
                    <Container fluid>
                        <h1 className="display-3" style={textStyle}>Bine ati venit, {localStorage["doctor"]}</h1>

                        <p  style={textStyle}> <b>Selectati ce doriti sa adaugati/modificati:</b> </p>

                        <p className="lead">
                            <Button color="primary" onClick={() =>window.open('http://localhost:3000/ingrijitor')}>Ingrijitor</Button>
                        </p>

                        <p className="lead">
                            <Button color="primary" onClick={() =>window.open('http://localhost:3000/pacient')}>Pacient</Button>
                        </p>

                        <p className="lead">
                            <Button color="primary" onClick={() =>window.open('http://localhost:3000/medicament')}>Medicamente</Button>
                        </p>
                    </Container>
                </Jumbotron>
            </div>
        )
        else{return null;}
    };
}

export default DoctorPage
